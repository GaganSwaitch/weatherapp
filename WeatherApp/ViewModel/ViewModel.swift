//
//  ViewModel.swift
//  WeatherApp
//
//  Created by GaGan on 4/4/22.
//

import Foundation
import RxSwift
import RxCocoa


class ViewModel {
    
    public enum HomeError {
        case internetError(String)
        case serverMessage(String)
    }
    
    public let lists : PublishSubject<[Lists]> = PublishSubject()
    public let weatherDetails : PublishSubject<[Weather]> = PublishSubject()
    public let temperatureDetails : PublishSubject<MainWeatherReport> = PublishSubject()
    public let loading: PublishSubject<Bool> = PublishSubject()
    public let error : PublishSubject<HomeError> = PublishSubject()
    
    private let disposable = DisposeBag()
    
    public func requestListData(){
        self.loading.onNext(true)
        
        WeatherService.load(CityListRequest()) { response in
            self.loading.onNext(false)
            self.lists.onNext(response?.list ?? [])
            
        }
    }
    
    public func requestDetailsData(id: Int){
        self.loading.onNext(true)
        
        WeatherService.load(WeatherDetailsRequest(cityID: id)) { response in
            self.loading.onNext(false)
            self.weatherDetails.onNext(response?.weather ?? [] )
            self.temperatureDetails.onNext(response?.main ?? MainWeatherReport.init(temp: 0, temp_min: 0, temp_max: 0, pressure: 0, sea_level: 0, grnd_level: 0, humidity: 0) )
        }
    }

    
}
