//
//  WeatherService.swift
//  WeatherApp
//
//  Created by GaGan on 4/4/22.
//

import Foundation

import RxSwift
import RxCocoa


protocol Request {
    var path: String { get }
    var method: Method { get }
    associatedtype Response: Decodable
}


enum Method {
    case get
    case post(Dictionary<String,Any>)
}

enum GetFailureReason: Int, Error {
    case unAuthorized = 401
    case notFound = 404
}


class WeatherService {
    
    static func load<T:Request>(_ weatherRequest: T, completion: @escaping (T.Response?) -> Void) {
        
        // Build Request
        let url = URL(string:"https://testapi.io/api/olestang/weather/" + weatherRequest.path)!
        
        var request = URLRequest(url: url)
        
        switch weatherRequest.method {
            // get data from URL
        case .get:
            request.httpMethod = "GET"
            
            //post data
        case .post(let payload):
            request.httpMethod = "POST"
            request.httpBody = try! JSONSerialization.data(withJSONObject: payload)
        }
        
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            //            Exception handling with do catch block
            do {
                let decoder = JSONDecoder()
                let messages = try decoder.decode(T.Response.self, from: data!)
                print(messages as Any)
                completion(messages)
                
            } catch DecodingError.dataCorrupted(let context) {
                print(context)
            } catch DecodingError.keyNotFound(let key, let context) {
                print("Key '\(key)' not found:", context.debugDescription)
                print("codingPath:", context.codingPath)
            } catch DecodingError.valueNotFound(let value, let context) {
                print("Value '\(value)' not found:", context.debugDescription)
                print("codingPath:", context.codingPath)
            } catch DecodingError.typeMismatch(let type, let context) {
                print("Type '\(type)' mismatch:", context.debugDescription)
                print("codingPath:", context.codingPath)
            } catch {
                print("error: ", error)
            }
        }.resume()
    }
    
    
    
}
