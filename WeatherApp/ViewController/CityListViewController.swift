//
//  ViewController.swift
//  WeatherApp
//
//  Created by GaGan on 4/4/22.
//

import UIKit
import RxSwift
import RxCocoa

class CityViewController: UIViewController {
    
    @IBOutlet weak var weatherListTableView: UITableView!
    public var lists = PublishSubject<[Lists]>()
    var viewModel = ViewModel()
    
//    DisposeBag allows us not to have to dispose of each subscription individually.
    private let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupBinding()
        viewModel.requestListData()
        
    }
    
    
    private func setupBinding(){
        
//               tableview cell register
        weatherListTableView.register(UITableViewCell.self,
                                      forCellReuseIdentifier: "TableViewCell")
        
        viewModel
            .lists
            .observe(on: MainScheduler.instance)
            .bind(to: self.lists)
            .disposed(by: disposeBag)
        
        weatherListTableView.rx.modelSelected(Lists.self)
            .subscribe(onNext: {[weak self] Lists in
                
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let detailsViewController = storyBoard.instantiateViewController(withIdentifier: "detailsViewController") as! DetailViewController
                
                detailsViewController.viewModel.requestDetailsData(id: Lists.id)
                self?.navigationController?.pushViewController(detailsViewController,
                                                               animated: true)
                
            })
            .disposed(by: disposeBag)
        
        
        lists.bind(to: weatherListTableView.rx.items(cellIdentifier: "TableViewCell")) { row, model, cell in
            cell.textLabel?.text = model.name
        }
        .disposed(by: disposeBag)
    }
    
}


