//
//  DetailViewController.swift
//  WeatherApp
//
//  Created by GaGan on 4/4/22.
//

import UIKit
import RxSwift
import RxCocoa

class DetailViewController: UIViewController{
  
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var weatherDescriptionLabel: UILabel!
   
    @IBOutlet weak var humidityLabel: UILabel!
    
    @IBOutlet weak var minTempLabel: UILabel!
    
    @IBOutlet weak var maxTempLabel: UILabel!
    
    public var weatherDetail = PublishSubject<[Weather]>()
    public var weatherTempDetail = PublishSubject<MainWeatherReport>()
    private let disposeBag = DisposeBag()
    var viewModel = ViewModel()
    var cityId: Int = 0
    
    override func viewWillAppear(_ animated: Bool) {
        setupBinding()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    
    private func setupBinding(){
        
        viewModel
            .weatherDetails
            .observe(on: MainScheduler.instance)
            .bind(to: self.weatherDetail)
            .disposed(by: disposeBag)
        
        
        viewModel
            .temperatureDetails
            .observe(on: MainScheduler.instance)
            .bind(to: self.weatherTempDetail)
            .disposed(by: disposeBag)
        
//        setting values for weatherDetail
        weatherDetail.bind { weatherDetail in
            self.tempLabel.text = weatherDetail[0].main
            self.weatherDescriptionLabel.text = weatherDetail[0].description
        }
        
//        setting values for weatherTempDetail
        weatherTempDetail.bind { weatherTempDetail in
            self.tempLabel.text =   String(format: "%.2f °C", weatherTempDetail.temp)
            self.humidityLabel.text = String(format: "%d", weatherTempDetail.humidity)
            self.minTempLabel.text =   String(format: "%.2f °C", weatherTempDetail.temp_min)
            self.maxTempLabel.text =   String(format: "%.2f °C", weatherTempDetail.temp_max)
        }
    }
}


