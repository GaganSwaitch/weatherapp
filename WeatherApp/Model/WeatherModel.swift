//
//  WeatherModel.swift
//  WeatherApp
//
//  Created by GaGan on 4/4/22.
//

import Foundation

struct CityListRequest: Request {
    typealias Response = CityList

    var method: Method = .get
    var path: String {
        return "list"
    }
   
}


struct WeatherDetailsRequest: Request {
    var method: Method = .get
    var path: String
    init(cityID: Int) {
        path = "\(cityID)"
    }
   typealias Response = WeatherDetails
}


struct CityList: Decodable {
    let list: [Lists]
}

struct Lists: Decodable {
    let id: Int
    let name: String
}

struct WeatherDetails: Decodable{
    var id: Int
    var name: String
    var main: MainWeatherReport
    var dt: Int
    var wind: Wind?
    var clouds: Clouds?
    var weather: [Weather]
}


struct Wind: Decodable{
    var speed: Double
    var deg: Double
}


struct Clouds: Decodable{
    var all: Int
}


struct MainWeatherReport: Decodable{
    var temp: Double
    var temp_min: Double
    var temp_max: Double
    var pressure: Double
    var sea_level: Double?
    var grnd_level: Double?
    var humidity: Int
}


struct Weather: Decodable {
    let id: Int
    let main: String
    let description: String
    let icon: String
}
